# HTTP协议
HTTP是不保存状态的协议。

#### HTTP方法
| 方法 | 介绍 |
| -- | -- |
| GET | 获取资源 |
| POST | 传输实体主体 |
| PUT | 传输文件/创建，更新资源 |
| HEAD | 和GET类似，但是不返回报文主体部分 |
| DELETE | 删除文件 |
| OPTIONS | 询问支持的方法（常见于CORS中的pre-flight模式） |
| TRACE | 追踪路径，让WEB服务器将之前的请求通信环回给客户端的方法 |
| CONNECT | 要求在与代理服务器通信时建立隧道，实现用隧道协议进行TCP通信。主要使用SSL和TLS 协议把通信内容加密后经网络隧道传输 |

####持久连接
持久连接：建立1次TCP连接后进行多次请求和响应的交互

HTTP/1.1中，所有默认连接都是持久连接。

####使用cookie的状态管理
Cookie会根据从serve发送的response报文内的Set-Cookie首部字段信息，通知客户端保存cookie。

当下次客户端再次往该server发送请求时，客户端会自动在request报文中加入Cookie值，然后发送。

